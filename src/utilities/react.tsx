
import React from "react";

/* eslint indent: ["error", "tab", { "FunctionExpression": {"body": 1, "parameters": 1} }] */
export const defaultProps = <
TPropsAll,
TPropsProvided extends Partial<TPropsAll>
>(
		Component: React.ComponentType<TPropsAll>,
		props: TPropsProvided
	): React.ComponentType<Omit<TPropsAll, keyof TPropsProvided>> => {
	const Inner = (additionalProps: Omit<TPropsAll, keyof TPropsProvided>) => (

		// @ts-expect-error - _typescript is unable to infer that `A & Omit<X, keyof A>` for `A extends X`
		// is assignable to any `X`
		<Component {...{ ...props, ...additionalProps }} />
	);

	return Inner;
};
