import Head from "next/head";
import React from "react";
import { websiteName } from "../../config/names";

export const Layout: React.FC<{
	title: string;
}> = ({ children, title }) => (
	<>
		<Head>
			<title>
				{`${title} | ${websiteName}`}
			</title>
			<link rel="icon" href="/static/favicon.ico" />
			<link href="https://fonts.googleapis.com/css2?family=Lato&family=Nunito:wght@600&family=Open+Sans:wght@600&family=Raleway&family=Roboto&family=Scope+One&display=swap" rel="stylesheet"/>
			<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;700&family=Lobster&display=swap" rel="stylesheet" />
		</Head>
		{children}
	</>
);
