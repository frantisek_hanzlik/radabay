import { css, useTheme } from "@emotion/react";
import styled from "@emotion/styled";
import React from "react";
import { SkewContainer } from "../../common/skew container";
import { SkewPadding } from "../../common/skew container/padding";
import { Timetable } from "./timetable";
import { skewAngleRad } from ".";

const skewContainerStyles = css`
	box-shadow: 0px 0px 25px 5px hsla(0, 0%, 0%, 0.8);
`;

const Container = styled(SkewContainer)`
	display: flex;
	justify-content: center;
`;

const Content = styled.div`
	padding: 2rem 0;
`;

export const Section1: React.FC = () => {
	const theme = useTheme();

	return (
		<Container
			as="section"
			width="100vw"
			backgroundColor={theme.pages.index.section1.background}
			skewContainerStyles={skewContainerStyles}
			angleRad={skewAngleRad}
		>
			<SkewPadding
				width="calc(20rem + 20vw)"
				angleRad={skewAngleRad}
				top
				bottom
			>
				<Content>
					<Timetable
						events={[
							{ time: "15:00", name: "začátek" },
							{ time: "15:00 - 19:00", name: "dětské atrakce" },
							{ time: "16:00 - 17:00", name: "soutěž o ceny" },
							{ time: "17:00 - 18:00", name: "orientační běh pro děti s rodiči" },
							{ time: "18:00 - 19:00", name: "koncert Jaroslava Uhlíře" },
						]}
					/>
				</Content>
			</SkewPadding>
		</Container>
	);
};
