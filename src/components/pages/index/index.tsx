import React from "react";
import { Layout } from "../../common/layout";
import { Header } from "./header";
import { Section0 } from "./section0";
import { Section1 } from "./section1";
import { Section2 } from "./section2";
import { Section3 } from "./section3";

export const skewAngleRad = -0.05;

export const Index: React.FC = () => (
	<Layout title="Úvod">
		<Header />
		<main>
			<Section0 />
			<Section1 />
			<Section2 />
			<Section3 />
		</main>
	</Layout>
);
