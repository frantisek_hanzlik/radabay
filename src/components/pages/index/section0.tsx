import styled from "@emotion/styled";
import React from "react";

const Container = styled.section`
	height: 100vh;

	display: flex;
	justify-content: center;
	align-items: flex-end;

	background:url("/static/corkboard.png");
	background-color: #6c9c1d;
	background-repeat: repeat;
`;

const Image = styled.img`
	width: calc(15rem + 50vw);

	box-shadow: 2px 2px 5px 0px hsla(0, 0%, 0%, 0.8);
`;

export const Section0: React.FC = () => (
	<Container>
		<Image src="/static/uvodni obrazek.webp" />
	</Container>
);
