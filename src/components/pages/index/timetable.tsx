import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { lighten } from "polished";
import React from "react";
import { fonts } from "../../../config/styles";

interface Event {
	time: string
	name: string
}

const Table = styled.table`
	width: 100%;

	border: 1rem solid ${({ theme }) => theme.pages.index.section2.background};
	border-radius: 2rem;
	border-spacing: 0;

	background-color: ${({ theme }) => lighten(0.3, theme.pages.index.section2.background)};

	font-size: 2rem;
	${fonts.caveat[400]}
`;

const cell = css`
	padding: 0.5rem 1rem;
`;

const DataCell = styled.td`
	${cell}

	text-align:center;

	&:first-child {
		white-space: nowrap;
	}
	&:not(:first-child) {
		border-left: 1px solid black;
	}
`;

const Header = styled.thead`
	font-weight: bold;
`;

const HeaderCell = styled.th`
	${cell}
	${fonts.caveat[700]}
`;

export const Timetable: React.FC<{
	events: Event[]
}> = ({ events }) => (
	<Table>
		<Header>
			<tr>
				<HeaderCell>
					Čas
				</HeaderCell>
				<HeaderCell>
					Akce
				</HeaderCell>
			</tr>
		</Header>
		<tbody>
			{events.map((event) => (
				<tr key={event.time + event.name}>
					<DataCell>
						{event.time}
					</DataCell>
					<DataCell>
						{event.name}
					</DataCell>
				</tr>
			))}
		</tbody>
	</Table>
);
