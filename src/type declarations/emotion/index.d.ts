import "@emotion/react";
import { Theme as ProjectTheme } from "../../config/theming";

declare module "@emotion/react" {
	// declaration merging does not work on type aliases and therefore interface is required.
	// eslint-disable-next-line @typescript-eslint/no-empty-interface
	export interface Theme extends ProjectTheme {}
}
