import { Global, css } from "@emotion/react";
import { normalize } from "polished";
import React from "react";
import { getFonts } from "../utilities/styles";

export const globalStyles = (
	<Global
		styles={css`
			${normalize()}

			:root {
				font-size: calc(10px + 1vw);
			}

			html,
			body {
				margin: 0;
				padding: 0;
			}

			*, *::before, *::after {
				box-sizing: border-box;
			}
		`}
	/>
);

export const fonts = {
	roboto: getFonts("'Roboto', sans-serif", [400]),
	nunito: getFonts("'Nunito', sans-serif", [600]),
	scopeOne: getFonts("'Scope One', serif", [400]),
	raleway: getFonts("'Raleway', sans-serif", [400]),
	openSans: getFonts("'Open Sans', sans-serif", [600]),
	lobster: getFonts("'Lobster', cursive", [400]),
	caveat: getFonts("'Caveat', cursive", [400, 700]),
};
