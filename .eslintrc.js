/* eslint-disable no-magic-numbers */

module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: ["@typescript-eslint"],
	extends: [
		"eslint:all",
		"plugin:react/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:import/errors",
		"plugin:import/warnings",
		"plugin:import/typescript",
	],

	parserOptions: {
		tsconfigRootDir: __dirname,
		project: ["./tsconfig.json"],
	},

	env: {
		es6: true,
		browser: true,
		jest: true,
		node: true,
	},
	settings: {
		react: {
			version: "detect",
		},
	},
	rules: {
		"@typescript-eslint/no-unused-vars": [
			2,
			{
				argsIgnorePattern: "^_",
			},
		],

		/**************/
		/* formatting */
		/**************/
		indent: ["error", "tab"],
		"no-tabs": "off",
		"one-var": ["error", "never"],
		"object-curly-spacing": ["error", "always"],
		"import/order": [
			"error",
			{ "newlines-between": "never", alphabetize: { order: "asc" } },
		],
		"padded-blocks": ["error", "never"],
		"quote-props": ["error", "as-needed"],
		"comma-dangle": ["error", "always-multiline"],
		"function-paren-newline": ["error", "multiline-arguments"],
		"space-before-function-paren": ["error", "never"],
		"array-element-newline": ["error", "consistent"],
		"spaced-comment": ["error", "always", { exceptions: ["*"] }],
		"object-property-newline": [
			"error",
			{ allowAllPropertiesOnSameLine: true },
		],
		"max-len": ["error", { code: 120, tabWidth: 0, ignoreUrls: true }],
		"array-bracket-newline": ["error", "consistent"],
		"function-call-argument-newline": ["error", "consistent"],
		"react/self-closing-comp": "error",
		"multiline-comment-style": ["error", "separate-lines"],
		"react/jsx-wrap-multilines": [
			"error",
			{
				declaration: "parens-new-line",
				assignment: "parens",
				return: "parens",
				arrow: "parens",
				condition: "ignore",
				logical: "ignore",
				prop: "ignore",
			},
		],
		"react/jsx-first-prop-new-line": ["error", "multiline"],
		"react/jsx-one-expression-per-line": "error",
		"implicit-arrow-linebreak": "off",
		"multiline-ternaries": "off",

		/**********************/
		/* confict resolution */
		/**********************/
		"no-extra-parens": ["error", "all", { ignoreJSX: "all" }],

		/****************************/
		/* handled by other tooling */
		/****************************/
		"react/prop-types": "off",
		"sort-imports": "off",

		/**************************************/
		/* @typescript-eslint extension rules */
		/**************************************/
		"no-use-before-define": "off",
		"@typescript-eslint/no-use-before-define": "error",

		/********/
		/* misc */
		/********/
		"capitalized-comments": ["error", "never"],
		// this rule does not take into account inheritance, so it causes false positives
		"class-methods-use-this": "off",
		// humans mostly sort object keys in a way that makes more sense than alphabetical sorting
		"sort-keys": "off",
		// this gets in the way when styling
		"no-magic-numbers": "off",
		// there's nothing bad about ternaries
		"no-ternary": "off",
	},
};
